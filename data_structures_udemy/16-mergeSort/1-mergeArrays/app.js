function mergeArrays(arr1,arr2) {
    let result = [];
    let i = 0;
    let j = 0;

    while ((i < arr1.length) && (j < arr2.length)) {
        //check which array has smallest elementa and push to result
        if (arr1[i] < arr2[j]) {
            result.push(arr1[i])
            i++;
        } else {
            result.push(arr2[j])
            j++;
        }
        //push rest of other array once one array has all elements pushed to result
        if (i === arr1.length) {
            for (let k = j; k < arr2.length; k++) {
                result.push(arr2[k])
            }
        }
        if (j === arr2.length) {
            for (let k = i; k < arr1.length; k++) {
                result.push(arr1[k])      
            }
        }
    }
    //edge case: if one array empty return non empty array
    if (arr1.length === 0) return arr2
    else if (arr2.length === 0) return arr1

    //returns result once arrays have been merged
    else return result
}

console.log(mergeArrays([100],[1,2,3,5,6]))