// 4-mini_project_book

import React from 'react';
import ReactDom from 'react-dom';

//CSS
//css file is imported same as it is done in javascript
import "./index.css";

//<Book/> is an instantiation of the Book JSX element defined below
function BookList() {
    return (
      <section className='booklist'>
        <Book/>
        <Book/>
        <Book/>
        <Book/>
        <Book/>
        <Book/>
        <Book/>
        <Book/>
        <Book/>
      </section>
    );
}

const Book = () => {
  return ( 
    <article className='book'>
      <Image/>
      <Title/>
      <Author/>
    </article>
  );
}

const Image = () => {
  return (<img src="https://images-na.ssl-images-amazon.com/images/I/811ftjOO5FL._AC_UL302_SR302,200_.jpg"
    alt=""/>
  );
}

const Title = () => {
  return <h2>Chicka Chicka Boom Boom</h2>;
}

const Author = () => {
  //typing curly braces {} changes from JSX to javascript syntax
  //below we are declaring an object with {} inside the curly braces that change the code to js
  return <h4 style={{ color: '#617d98',fontSize:'0.75rem',marginTop: '0.25rem'}}>Bill Martin Jr.</h4>;
}


ReactDom.render(<BookList />, document.getElementById('root'));