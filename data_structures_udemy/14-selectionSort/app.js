function selectionSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        let min = i
        for (let j = i; j < arr.length; j++) {
            if (arr[j] < arr[min]) {
                min = j
            }   
        }
        if (min !== i) {
            let temp = arr[i]
            arr[i] = arr[min];
            arr[min] = temp;
        }
    }

    return arr;
}

console.log(selectionSort([2,6,3,6,3,2,5,3,23,5,6,67,5,3,5,6,432,33,2,33,33,213,424,2,34,4,2,4,2,4,2,432]))