/* PLAN
- set subStart End, Total, and Length
- start for loop to iterate through array
- use sliding window to first check for 1 element subarray that meets target
  then move to 2 element subarray etc until smallest subarray is found
- return length when subarray found or 0 is whole array does not equal target

- start window as whole array check if reaches target
- if it does shrink window by one and check if it reaches target
- if it does shrink again and repeat if not return current subarray size


*/

// function maxSubarrayLen(arr,target) {
//   if (arr.length == 0) {
//     return null;
//   }

//   let subStart = 0;
//   let subEnd = len - 1;
//   let subTotal = 0;
//   let targetReached = false;

//   for (let i = 0; i < array.length; i++) {
//     const element = array[i];
    
//   }

//   return 0
// }

function minSubArrayLen(nums, sum) {
  let total = 0;
  let start = 0;
  let end = 0;
  let minLen = Infinity;
 
  while (start < nums.length) {
    // if current window doesn't add up to the given sum then 
		// move the window to right
    if(total < sum && end < nums.length){
      total += nums[end];
			end++;
    }
    // if current window adds up to at least the sum given then
		// we can shrink the window 
    else if(total >= sum){
      minLen = Math.min(minLen, end-start);
			total -= nums[start];
			start++;
    } 
    // current total less than required total but we reach the end, need this or 
    // else we'll be in an infinite loop 
    else {
      break;
    }
  }
 
  return minLen === Infinity ? 0 : minLen;
}

console.log(minSubArrayLen([2,3,1,2,4,3],7));
console.log(minSubArrayLen([2,1,6,5,4],9));
console.log(minSubArrayLen([3,1,7,11,2,9,8,21,62,33,19],52));
console.log(minSubArrayLen([1,4,16,22,5,7,8,9,10],39));
console.log(minSubArrayLen([1,4,16,22,5,7,8,9,10],55));
console.log(minSubArrayLen([4,3,3,8,1,2,3],11));
console.log(minSubArrayLen([1,4,16,22,5,7,8,9,10],95));
