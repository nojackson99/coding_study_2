import ReactDom from 'react-dom';
import React from 'react';  //needed if manually creating a react element

// creates a jsx element that can be rendered with ReactDom
// must always return something or else error will occur
function Greeting() {
  return (
  <div> 
    <h4>this is Noah and this is my first component</h4>;
  </div>
  );
}

// showing a manual way to create react element using React.createElement
// here using a const with an arrow function instead of named function
const Greeting1 = () => {
  return React.createElement('h1', {}, 'hello world');
}

// jsx is esspecially useful for creating components with multiple html elements
// below is manual component with 2 html elements
const Greeting2 = () => {
  // nested React.createElement
  // less intuitive than using JSX as shown in Greeting()
  return React.createElement(
    'div',
    {},
    React.createElement('h1', {}, 'hello world'))
}

// rendering our jsx element created in Greeting()
// first argument is funciton containing jsx
// second is targeting a dom element
ReactDom.render(<Greeting/>, document.getElementById('root'));