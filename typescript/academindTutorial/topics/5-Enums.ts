// Special type only in typescript that assigns labels to numbers
// In declaration below ADMIN is 0, READ_ONLY is 1, etc.
// Best practice is to use all uppercase for enum label names
// Example for when to use is when checking user permission
enum Role { ADMIN, READ_ONLY, AUTHOR };

const person = { 
    name: 'Noah',
    age: 22,
    hobbies: ['Sports', 'Cooking'],
    role: Role.ADMIN
}

if (person.role === Role.ADMIN) {
    console.log('is admin');
}