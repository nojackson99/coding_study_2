// Date object

const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'    
];

const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
];

// includes many methods to get different date formats
const date = new Date();
// console.log(date);

// this will return number correspoding to month number
// still zero indexed so 0 is january, 11 is december, etc
// const month = date.getMonth();
// console.log(month);

// returns day of the week
// const day = date.getDay();
// console.log(day);



//exercise to write out giveaway end date

const minutes = date.getMinutes();
console.log(minutes);

const hours = date.getHours() - 12;
console.log(hours);

const time = `${hours}:${minutes}pm`
console.log(time);

const day = days[date.getDay()];
console.log(day);

const month = months[date.getMonth()];
console.log(month);

const fullYear = date.getFullYear();
console.log(fullYear)

const sentence = `<h1>Giveaway Ends On ${day}, ${date.getDate()} ${month} ${fullYear} ${time}</h1>`

document.body.innerHTML = sentence


