import React from 'react';

// goal here is to create a button that will change title and display the change to the page
const ErrorExample = () => {
  let title = 'random title';
  const handleClick = () => {
    // this will change title to hello people but will not trigger the component to rerender
    title = 'hello people'
  }
  return (
    // react fragment allows us to create a component with multipe elements without a parent elemnt
    // this prevents a div within a div when components are rendered on the page
    <React.Fragment>
      <h2>{title}</h2>
      <button type='button' className="btn" onClick={handleClick}>
        change title
      </button>
    </React.Fragment>
  )
};

export default ErrorExample;
