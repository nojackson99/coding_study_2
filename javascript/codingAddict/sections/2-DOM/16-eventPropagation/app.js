// event propagation
// allows for selecting dynamic elements (eg. elements added by javascript)

const container = document.querySelector('.container');
const list = document.querySelector('.list-items');


function showBubbling(event){
    console.log('current target',event.currentTarget);
    console.log('current target',event.target);
    
    if(event.target.classList.contains('link')){
        console.log('you clicked on the link')
    }
}

function stopPropagation(event){
    event.stopPropagation();
}

//due to event bubbling eventListener will first fire on the link
//clicked within the list or container
list.addEventListener('click',stopPropagation, { capture: true})
container.addEventListener('click',showBubbling, { capture: true})
document.addEventListener('click',showBubbling, { capture: true})
window.addEventListener('click',showBubbling, { capture: true})