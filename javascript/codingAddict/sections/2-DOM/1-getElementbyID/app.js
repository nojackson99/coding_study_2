// get element by ID

// storing html element in variable
const h1 = document.getElementById('title');
h1.style.color = 'red';

// changing html element without storing in variable
// must type document.getElementById(<id>) each time
document.getElementById('btn').style.backgroundColor = 'blue';
document.getElementById('btn').style.color = 'white';


