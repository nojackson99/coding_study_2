// prevAndNextSibling

// use console log if you are unsure what element you are selecting
const first = document.querySelector('.first')

console.log(first);

// to grab next element in list use .nextSibling twice to move past whitespace that is between list elements
const second = (first.nextSibling.nextSibling.style.color = 'red');
console.log(second);

const last = document.querySelector('#last');
console.log(last);
last.style.color = 'blue'

// syntax is simlar for finding previous slbiings
// use .previousSibling


