// 6-props

import React from 'react';
import ReactDom from 'react-dom';

// CSS
// css file is imported same as it is done in javascript
import "./index.css";

// variables
const author = 'Bill Martin Jr.';
const title =  'Chicka Chicka Boom Boom';
const img = "https://images-na.ssl-images-amazon.com/images/I/811ftjOO5FL._AC_UL302_SR302,200_.jpg";

//<Book/> is an instantiation of the Book JSX element defined below
function BookList() {
    return (
      <section className='booklist'>
        {/* instance passed into Book as a prop */}
        <Book instance={1} text='first element'/>
        <Book instance={2} text='second element'/>
        
        <Book instance={3} text='thrid element'>

        {/* child jsx element passed in as children */}
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsa, quos.</p>
        
        </Book>
      </section>
    );
}

// Book can accept arguments sent it on instantiation called properites
// props can be passed in than accessed through the props object
// const Book = ( {props} ) = > {
// or be structured and accessed directly
const Book = ( {instance, text,children}) => {
  // can also destructure withing the function if passing with "props"
  // const { instance, text}) = props;
  return ( 
    <article className='book'>
      <img src= {img} alt=""/>
      {/* here we use {} to switch to javascript and use the js constants in these jsx elements*/}
      <h2>{title}</h2>
      <h4>{author}</h4>
      {/* access probs with props."variable name" */}
      {/* <p>this is jsx element # {props.instance}</p> */}

      {/* here we are accessing the destructured props directly */}
      <p>this is jsx element # {instance}</p>
      <p>{text}</p>

      {/* children accessed directly and only displayed on the elments where it was passed in */}
      {children}
    </article>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));