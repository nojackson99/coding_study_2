function recursiveInsertionSort(arr,n) {
    if (n <= 1) {

        return
    }


    //console.log(`calling again \n n called at: ${n-1}`);
    recursiveInsertionSort(arr,n-1);

    //first run
    console.log(`before sorting: ${arr}`);
    let cur = arr[n-1]; 
    console.log(`current is: ${cur}`);
    let j = n-2;         
    console.log(`j is      : ${j}`)

    while ((j >= 0) && (cur < arr[j])) {
        arr[j+1] = arr[j];
        j--;
    }
    arr[j+1] = cur;
    console.log(`after sorting: ${arr}`);
    console.log('\n\n')
}

array = [5,2,9,7,14,8]
recursiveInsertionSort(array,array.length)
console.log(array)