// prototypal inheritance model

function Account(name, initialBalance) {
    this.name = name;
    this.balance = initialBalance;
    // creating functions with each object is inefficient
    // this.deposit = function(amount){
    //     this.balance += amount;
    //     console.log(`Hello ${this.name}, your balance is ${this.balance}`);
    // }
}

const noah = new Account(`noah`, 500);
const alyssa = new Account(`alyssa`, 700);

Account.prototype.bankName = 'Huntington'

Account.prototype.deposit = function(amount){
        this.balance += amount;
        console.log(`Hello ${this.name}, your balance is ${this.balance}`);
    }

noah.deposit(300);
alyssa.deposit(1000);