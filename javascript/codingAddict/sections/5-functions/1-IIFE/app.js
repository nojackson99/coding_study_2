// Immediately-invoked function expression
// replaced by modules

const num1 = 20;
const num2 = 10;

function add() {
    console.log(`the result is : ${num1 + num2}`)
}
add();

// iffe syntax here
// useful because num3 and num4 are restrained to scope of iffe function
// this prevents accidental reassignment and allows or these variable names to be
// used again in global or another local scope
(function(){
    const num3 = 30;
    const num4 = 50
    console.log(`the result is : ${num3 + num4}`);
})()
