//store button elements
resetBtn = document.querySelector(".reset");
increaseBtn = document.querySelector(".increase");
decreaseBtn = document.querySelector(".decrease");

counterDisplay = document.getElementById("value");

//checks counter and changes value according to event caller
function checkCounter(event) {
    let temp = counterDisplay.textContent;
    console.log(event.currentTarget.buttonFlag);

    //changes counter value according to buttonFlag
    switch(event.currentTarget.buttonFlag){
        case 1:
            temp++;
            if (temp > 0) {
                counterDisplay.style.color = 'green'
            }
            break;
        case 2:
            temp--;
            if (temp < 0) {
                counterDisplay.style.color = 'red'
            }
            break;
        default:
            temp = 0;
            counterDisplay.style.color = 'black'
            break;
    }

    console.log(`temp: ${temp}`);
    counterDisplay.textContent = temp;

    // final check to fix not checking if zero bug
    if (counterDisplay.textContent == 0) {counterDisplay.style.color = 'black'} 

}

resetBtn.addEventListener('click',checkCounter,false)
resetBtn.buttonFlag = 0;

increaseBtn.addEventListener('click',checkCounter,false)
increaseBtn.buttonFlag = 1;

decreaseBtn.addEventListener('click',checkCounter,false)
decreaseBtn.buttonFlag = 2;



