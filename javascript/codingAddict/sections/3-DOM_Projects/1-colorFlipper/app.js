const colors = ["yellow", "blue", "purple", "green", "white", "black"];    //array of possible colors
const myBtn = document.getElementById('btn');
// const pageBackground = document.querySelector('main');
const colorTextDisplay = document.querySelector('.color');

function buttonCallback() { 
    //newColor set to random element from colors array based on return form getRandomNumber()
    let newColor = colors[getRandomNumber()];
    colorTextDisplay.textContent = newColor;    //switch display to show what color is in background

    // my verison
    // pageBackground.style.backgroundColor = newColor;

    // better code
    // body is a method of document and does not need to be targeted with querySelector
    document.body.style.backgroundColor = newColor;
}

// generate random number between 0 and colors array length
function getRandomNumber() {
    min = Math.ceil(0);
    max = Math.floor(colors.length);
    return Math.floor(Math.random() * (max - min) + min);
}

// on click of myBtn buttonCallback function will be executed
myBtn.addEventListener('click',buttonCallback)

