// Array methods

// reduce
// iterates, callback funciton
// reduces to a single value - number, array, object
// 1st parameter ('acc') - total of all calculations | accumulator
// 2nd parameter ('curr') - current iteration/value | current item

const people = [
    {name: 'Noah',age: 22,job: 'developer', id:1, salary:300},
    {name: 'Alyssa',age: 22,job: 'total_babe', id:2, salary:200},
    {name: 'Ami',age: 1,job: 'dog', id:3, salary:30},
    {name: 'Andrew', age: 22,job: 'journalist', id:4, salary:100}
];

// reduce has two arguments
//   -the function code that will do the reducing
//   -and the type that will reuturn
//      -in this case return type is number but it can be object {}, array [], other...
const total = people.reduce(function(acc, currItem){
    //shows total as reduce iterates through array
    console.log(`total: ${acc}`);

    //shows current item that is being manipulated
    console.log(`current money: ${currItem.salary}`);

    acc += currItem.salary
    
    //always return accumulator
    return acc;     //this return will track the running total
},0)