// this advanced

// points to global window object
// console.log(this);

function showThis() {
    console.log(this);
}

const john = {
    name:'john',
    showThis: showThis,
};

const bobo = {
    name: 'bob',
    showThis: showThis,
};

// invoking showThis will show object that object resides in
john.showThis();
bobo.showThis();

// invoking showThis from main will this will point to global window object
showThis();
