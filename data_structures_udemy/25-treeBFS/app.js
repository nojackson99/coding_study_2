class Node {
    constructor(val){
        this.value = val;
        this.left = null;
        this.right = null;
    }
}

class BinarySearchTree {
    constructor(){
        this.root = null;
    }
    insert(val){
        let node = new Node(val);
        if(!this.root){
            this.root = node;
            return this;
        } else {
            let current = this.root;
            while(current){
                if (val === current.value) return undefined;
                if (val < current.value) {
                    if (!current.left){
                        current.left = node; 
                        break;
                    } 
                    else current = current.left;
                } 
                else {
                    if (!current.right) {
                        current.right = node;
                        break;
                    } 
                    else current = current.right;
                }
            }
            return this;
        }
    }
    find(val) {
        if(!this.root) return false
        let current = this.root
        while(true) {
            if (!current) return false
            if (current.value === val) return current;
            if(val > current.value) current = current.right
            else current = current.left
        }
    }
    //bredth first search
    BFS(){
        let myQueue = [];
        let nodesVisited = [];
        let tempNode = null;

        myQueue.unshift(this.root);

        while(myQueue.length) {
            tempNode = myQueue.pop();
            nodesVisited.push(tempNode.value);

            if (tempNode.left) myQueue.unshift(tempNode.left);
            if (tempNode.right) myQueue.unshift(tempNode.right);
        }

        return nodesVisited;
    }
}

var tree = new BinarySearchTree();
tree.insert(10);
tree.insert(5);
tree.insert(13);
tree.insert(11)
tree.insert(2)
tree.insert(16)

// console.log(tree);
console.log(tree.BFS());


