import React from 'react';
import { data } from '../../../data';

const UseStateArray = () => {
  // here we are calling use state with React.useState
  // this can be done instead of importing
  const [people, setPeople] = React.useState(data)

  // removeItem is a click event reference function
  // accepts id and filters through people[]
  // .filter is used to filter out people elemnts where id mathces idToRemove
  // setPeople is used to update state of people with new list
  const removeItem = (idToRemove) => {
    let newPeople = people.filter((person) => person.id !== idToRemove);
    setPeople(newPeople);
  }
  return (
    // angle brackets <> </> is the same as <React.Fragment>
    <>
      {
        people.map((person) => {
          // destructure person object
          const { id, name } = person;
          return (
            // lists require a unique identifier key
            <div key={id} className='item'>
              <h4>{name}</h4>
              <button onClick={() => removeItem(id)}>
                clear item
              </button>
            </div>
          )
        })
      }
      {/* deletes displayed names by updating state of people to empty array [] 
          here we are using the in-line method as opposed to reference method */}
      <button className='btn' onClick={() => setPeople([])}>
        clear list
      </button>
    </>
  )
}
export default UseStateArray
