//Function types:
//functions have return types just like variables
//types are inferred by typescript or can be defined by the coder
//type for add function is number and type for printResult function is void because printResult is not returning anything
function add(n1, n2) {
    return n1 + n2;
}
//void function used when function does not return anything
//undefined is also a type but is not used often
function printResult(num) {
    console.log('Result: ' + num);
}
printResult(add(5, 12));
var combineValues;
combineValues = add;
console.log(combineValues(8, 8));
console.log(add(8, 8));
