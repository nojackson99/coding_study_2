function radixSort(arr) {
    function getDigit(num, i) {
        return Math.floor(Math.abs(num) / Math.pow(10, i)) % 10;
    }
      
    function digitCount(num) {
        if (num === 0) return 1;
        return Math.floor(Math.log10(Math.abs(num))) + 1;
    }
      
    function mostDigits(nums) {
        let maxDigits = 0;
        for (let i = 0; i < nums.length; i++) {
            maxDigits = Math.max(maxDigits, digitCount(nums[i]));
        }
        return maxDigits;
    }

    const largetNum = mostDigits(arr);

    for (let i = 0; i < largetNum; i++) {
        let bucketsArray = Array.from({length: 10}, () => [])

        for (let j = 0; j < arr.length; j++) {
            const digit = getDigit(arr[j],i)
            bucketsArray[digit].push(arr[j]);
        }

        arr = [];

        for (let k = 0; k < 10; k++) {
            arr = arr.concat(bucketsArray[k])
        }
    }

    return arr;
}

let myArray = [70,54,91,25,76,51,13,80,48,13]
console.log(myArray);
let newArray = radixSort(myArray);
console.log(newArray);

