// Math object

const number = 25

// .floor rounds number down to nearest whole number
let result = Math.floor(number);

// rounds number up
result = Math.ceil(number);

result = Math.sqrt(number);
result = Math.PI;

// find min and max of list of items
result = Math.max(4, 5, 6, 7);
result = Math.min(4, 5, 6, 7);

// gives random number between 0 and .99999999999
// can multiply after the fact to get random whole numbers
// combine with ceil or floor to get rid of post decimal numbers
result = Math.random();

console.log(result);