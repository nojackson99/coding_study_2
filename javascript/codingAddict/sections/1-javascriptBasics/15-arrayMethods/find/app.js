// Array methods

// find
// returns single item - (object in this case)
// returns first match, undefined if no match found
// great for getting unique value

const people = [
    {name: 'Noah',age: 22,job: 'developer', id:1},
    {name: 'Alyssa',age: 22,job: 'total_babe', id:2},
    {name: 'Ami',age: 1,job: 'dog', id:3},
    {name: 'Andrew', age: 22,job: 'journalist', id:4}
];

const subjects = ['math', 'english', 'science'];

const not_a_person = people.find(function(person){
    return person.job === 'dog';
});

console.log(not_a_person);

console.log(subjects.find(function(subjects){
    return subjects === 'english'
}))
