let myArray = [4,8,2,1,5,7,6,3]

function pivot(arr,start = 0,end = arr.length + 1) {
    function swap(arr,a,b) {
        let c = arr[a];
        arr[a] = arr[b];
        arr[b] = c;
    }

    let myPivot = arr[start];
    let pivotIndex = start;

    for (let i = start + 1; i < arr.length; i++) {
        if (myPivot > arr[i]) {
            pivotIndex++;
            swap(arr,i,pivotIndex);
        }
    }

    swap(arr,start,pivotIndex)
    return pivotIndex

}

function quickSort(arr, left = 0, right = arr.length - 1) {
    if(left < right) {
        let pivotIndex = pivot(arr, left, right)
        quickSort(arr,left,pivotIndex - 1);
        quickSort(arr,pivotIndex + 1,right);
    }

    return arr;
}

console.log(myArray);
myArray = quickSort(myArray,startIndex,endIndex)
console.log(myArray);

