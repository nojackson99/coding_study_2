// Array methods

// filter
// does return a new array
// can manipulate the size of new array
// returns based on condition

const people = [
    {name: 'Noah',age: 22,job: 'developer'},
    {name: 'Alyssa',age: 22,job: 'total_babe'},
    {name: 'Ami',age: 1,job: 'dog'},
    {name: 'Andrew', age: 22,job: 'journalist'}
];

const youngPeople = people.filter(function(person){
    return person.age <= 5;
});

const youngAdults = people.filter(function(person){
    return person.age === 22;
})

console.log(youngPeople);
console.log(youngAdults);