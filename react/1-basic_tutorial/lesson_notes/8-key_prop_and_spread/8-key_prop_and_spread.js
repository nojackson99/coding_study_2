// 8-key_prop_and_spread_operator

import React from 'react';
import ReactDom from 'react-dom';

// css
import "./index.css";

// variables
// to simplify component rendering we turn book objects into an array
// we use map in the BookList to assist in constructing Book components
const books = [
  {
    id: 1,
    img:
      'https://images-na.ssl-images-amazon.com/images/I/811ftjOO5FL._AC_UL302_SR302,200_.jpg',
    title: 'Chicka Chicka Boom Boom',
    author: 'Bill Martin Jr.'
  },
  {
    id: 2,
    img:
      'https://images-na.ssl-images-amazon.com/images/I/81EVdWdmOKL._AC_UL604_SR604,400_.jpg',
    title: 'Brown Bear, Brown Bear, What Do You See?',
    author: 'Bill Martin Jr / Eric Carle'
  },
  {
    id: 3,
    img:
      'https://images-na.ssl-images-amazon.com/images/I/91cqEsyjd-L._AC_UL604_SR604,400_.jpg',
    title: 'The Very Hungry Catepillar',
    author: 'Eric Carle'
  }
]

function BookList() {
  return (
    <section className='booklist'>
      {/* map allows us to pass in each element of books as a book property into our Book JSX element */}
      {books.map((book) => {
        // KEY
        // each book component is required to have a unique key value
        // this is fixed with key={book.id}
        // when working with api to build components a key will usually already exist
        // SPREAD OPERATOR
        // spread operator ... passes each property of book object in as props
        return <Book key={book.id} {...book}></Book>
      })}
    </section>
  );
}

const Book = (props) => {
  console.log(props);
  // spread operator allows us to change "props.book" to "props"
  // as now props contians three separate variables instead of contianing a book object that must be accessed
  const { img, title, author } = props;
  return (
    <article className='book'>
      <img src={img} alt="" />
      <h2>{title}</h2>
      <h4>{author}</h4>
    </article>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));