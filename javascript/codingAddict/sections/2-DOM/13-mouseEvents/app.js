// mouse events

//mousedown | mouseup | mouseenter | mouseleave

const eventsHeading = document.querySelector('h2');
const myBtn = document.querySelector('.btn');

//fires third
myBtn.addEventListener('click', function(){
    console.log('you clicked me');
})

//fires first
myBtn.addEventListener('mousedown', function(){
    console.log('down');
})

//fires second
myBtn.addEventListener('mouseup', function(){
    console.log('up');
})

eventsHeading.addEventListener('mouseenter',function(){
    eventsHeading.classList.add('blue');
})

eventsHeading.addEventListener('mouseleave',function(){
    eventsHeading.classList.remove('blue');
})