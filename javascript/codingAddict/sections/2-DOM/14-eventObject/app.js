// event object

const heading = document.querySelector('h1');
const button = document.querySelector('.btn');
const link = document.getElementById('link');


heading.addEventListener('click',function(event){
    //targets the current element from the event listener
    //callback paramater can be anything
    event.currentTarget.classList.add('blue');
    
    //can also use this instead but be careful
    //this would not work if we use arrow function (=>) instead of function(event)
    console.log(this);
})

link.addEventListener('click', function(event) {
    //clinking links normally brings us to the top of the page
    //.preventDefualt() will stop this behvior from occuring
    event.preventDefault();
})

//this keyword

