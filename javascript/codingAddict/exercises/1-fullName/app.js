// Arrays and for loop
// Exericse using for loop and template literal to add last name to each element of array and push to new array

//create vars for first names array, last name to add, and new array
const names = ['Noah', 'Ami', 'Chris', 'Lauren', 'Ron'];
const lastName = 'Jackson';
const fullNames = [];

//iterate over each element of array names
for (let index = 0; index < names.length; index++) {
    //combine element of names to lastName
    const combinedName = `${names[index]} ${lastName}`;
    //push this to new array fullNames
    fullNames.push(combinedName);
}

//console log final result of fullNames to check for corrct logic
console.log(fullNames);

