function bubbleSort(arr) {
    let swapMade = null

    for (let i = arr.length - 1; i > 0; i--) {
        swapMade = false;
        for (let j = 0; j < i - 1; j++) {
            if (arr[j] > arr[j+1]) {
                let temp = arr[j];
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
                swapMade = true;
            }
        }
        if (!swapMade) {
            console.log(`Breaking at: ${i}`);
            break;
        }
    }
    return arr;
}

console.log('hello world');
console.log(bubbleSort([2,6,3,6,3,2,5,3,23,5,6,67,5,3,5,6,432,33,2,33,33,213,424,2,34,4,2,4,2,4,2,432]))