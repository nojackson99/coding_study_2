// Conditional Statements
// >, <, >=, <=, ==, ===, !=, !===

if(true){       // explicitly true, always passes
    console.log('hello world');
}

const value = 2 > 1; //statment is evaluated and value is a boolean type
//because value is evaluated as true this check will pass
if(value) {
    console.log('value is true yay!!')
}

const value2 = 1 > 2;
if(value2) {
    console.log('hello world');
} else {
    console.log('value2 is false Boooo!');
}

//------------------------------------------------------------------------------------------------------------
// <, >, if else

const num1 = 4;
const num2 = 6;

if(num1 > num2){
    console.log('num1 bigger than num2');
} else if (num2 > num1) {
    console.log('num2 bigger than num1');
}
else {
    console.log('num1 and num2 are equal');
}

//------------------------------------------------------------------------------------------------------------
// not operator: !

const val = true;

//checks if not val or if val is false
if(!val) {
    console.log('val is true');
};

//------------------------------------------------------------------------------------------------------------
// equality ==, ===, !=, !==

const number = 6;
const number2 = '6';

const v = number == number2 // = is assignment operator, == check if left is equal to right
const v2 = number2 === number; // === also checks for left right equality but also checks for type equality

// In this example v is true but v2 is not because === checks datatype and number is a number while number2 is a string 
console.log(v);
console.log(v2);

// != is the not operator for == and
// !== is the not operator for ===