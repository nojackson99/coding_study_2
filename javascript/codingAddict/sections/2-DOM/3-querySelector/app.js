// querySelector or querySelectorAll

//this selects id of the ul tag in index
//# is needed for selecting the id
//for grabbing a tag or class use . ex .h1 .h2 .<class-name> etc
const result = document.querySelector('#result');
result.style.backgroundColor = 'blue';

const item = document.querySelectorAll('.special');
console.log(item);

const lastItem = document.querySelector('li:last-child');
console.log(lastItem);

//querySlectorAll alows for using forEach to modify captured elements
const list = document.querySelectorAll('.special');

list.forEach(function(item){
    console.log(item);
    item.style.color = 'yellow'
})

