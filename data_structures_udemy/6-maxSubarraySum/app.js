//plan

function maxSubarraySum(arr,len) {
  if (len > arr.length) {
    return null;
  }

  let subStart = 0;
  let subEnd = len - 1;
  let subTotal = 0;

  for (let i = 0; i < len; i++) {
    subTotal = subTotal + arr[i];
  }
  max = subTotal;

  while (subStart < (arr.length - len + 1)) {
    subEnd++;
    subTotal = subTotal - arr[subStart] + arr[subEnd];
    subStart++;
    if (subTotal > max) {
      max = subTotal;
    }
  }

  return max
}

// console.log(maxSubarraySum([100,200,300,400],2));
console.log(maxSubarraySum([1,4,2,10,23,3,1,0,20],4));
// console.log(maxSubarraySum([-3,4,0,-2,6,-1],2));
// console.log(maxSubarraySum([3,-2,7,-4,1,-1,4,-2,1],2));
// console.log(maxSubarraySum([2,3],3));
