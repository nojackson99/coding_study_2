// clickEvents

// core princables
//   1. select element
//   2. addEventListener()
//   3. what event, what to do

const myBtn = document.querySelector(".btn")
const myBtn2 = document.querySelector(".btn2")

const eventsHeading = document.querySelector('h2')
// console.log(eventsHeading);

function changeColors(){
    //using class list to check if h2 has red class
    let hasClass = eventsHeading.classList.contains('red'); //check with .classList.contains

    if(hasClass){
        eventsHeading.classList.remove('red');
    }
}

//1st param is type of event
//2nd param is callback funciton with code that will execute upon event trigger
//  can be formally declared or annonymous
myBtn.addEventListener('click',function(){
    // console.log(myBtn);
    eventsHeading.classList.add('red');
});

//here the callback function is passed as reference
myBtn2.addEventListener('click',changeColors)