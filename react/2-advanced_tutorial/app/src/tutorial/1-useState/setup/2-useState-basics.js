// use state returns two values | [undefined, f]
// value that we will use and function that will control the value
import React, { useState } from 'react';

const UseStateBasics = () => {
  // console.log(useState('hello world'))
  // const value = useState(1)[0];
  // const handler = useState(1)[1];
  // console.log(value, handler)

  // first value can have any name
  // common convention says to name second value set"first value"
  const [text, setText] = useState('random title')

  // setText function changes the value of text
  // beecause we are using useState hook the component is rerendered
  const handleClick = () =>{
    if(text === 'hello world') {
      setText('random title')
    } else {
      setText('hello world')
    }
  }
  return (
    <React.Fragment>
      <h1>{text}</h1>
      <button className="btn" onClick={handleClick}>
        change title
      </button>
    </React.Fragment>
    )
};

export default UseStateBasics;
