function naiveSubStringSearch(string,sub) {
    let counter = 0;

    for (let i = 0; i < string.length; i++) {
        if (string[i] === sub[0]) {
            for (let j = 1; j < sub.length; j++) {
                if (sub[j] !== string[i + j]) {
                    break;
                }
                if (j === sub.length - 1) {
                    counter++;
                }            
            }
        }   
    }
    return counter;
}

console.log(naiveSubStringSearch('lol lorie loled','lol'));