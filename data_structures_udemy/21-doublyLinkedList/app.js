class Node{
    constructor(val){
        this.val = val;
        this.next = null;
        this.prev = null;
    }
}

class DoublyLinkedList {
    constructor(){
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    push(val){
        let node = new Node(val);

        if (!this.head) {
            this.head = node;
            this.tail = node;
        } else {
            this.tail.next = node;
            node.prev = this.tail;
            this.tail = node;
        }

        this.length++;
        return this;
    }
    pop(){
        if (!this.tail) return undefined;

        let oldTail = this.tail

        // list has one node, set to empty list
        if (this.length ===  1) {
            this.tail = null;
            this.head = null;
        } else {
            this.tail = oldTail.prev;
            this.tail.next = null;
            oldTail.prev = null;    //ensures list can not be accessed from popped node
        }
       
        this.length--;
        return oldTail;

    }
    shift(){
        if (!this.head) return undefined;

        let oldHead = this.head;

        if (this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = oldHead.next;
            this.head.prev = null;
            oldHead.next = null;
        }

        this.length--;
        return oldHead;
    }
    unshift(val){
        let newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.head.prev = newNode;
            newNode.next = this.head;
            this.head = newNode;
        }

        this.length++;
        return this;
    }
    get(pos){
        if (pos < 0 || pos >= this.length) return null
        let nodeReturn = null
        // if desired element is in first half of list traverse from start
        if (pos <= Math.floor(this.length/2)) {
            nodeReturn = this.head;
            for (let i = 0; i < pos; i++) nodeReturn = nodeReturn.next
        } 
        // if desired element is in second half of list travers from end
        else { 
            nodeReturn = this.tail
            for (let i = this.length - 1; i > pos; i--) nodeReturn = nodeReturn.prev;
        }
        return nodeReturn;
    }
    set(val,pos) {
        let node = this.get(pos);
        if (node) {
            node.val = val;
            return true;
        } else return false;
    }
    insert(val,pos) {
        if (pos < 0 || pos > this.length) return false
        else if (pos === 0) return !!this.unshift(val);
        else if (pos === this.length) return !!this.push(val);
        else {
            let newNode = new Node(val);
            let nextNode = this.get(pos);
            let prevNode = nextNode.prev;

            prevNode.next = newNode;
            newNode.prev = prevNode;
            newNode.next = nextNode;
            nextNode.prev = newNode;

            this.length++;
            return true;
        }

    }
    remove(pos) {
        if (pos < 0 || pos >= this.length) return false
        else if (pos === 0) return this.shift();
        else if (pos === this.length - 1) return this.pop();
        else {
            let foundNode = this.get(pos);
            foundNode.prev.next = foundNode.next
            foundNode.next.prev = foundNode.prev
            foundNode.prev = null;
            foundNode.next = null;

            this.length--;
            return foundNode;
        }
    }
}

let myList = new DoublyLinkedList();

myList.push("Harry")
myList.push("Ron");
myList.push("Hermione");

