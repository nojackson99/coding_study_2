function power(base,exp) {
    if (exp === 0) {
        return 1;
    } else if (exp === 1) {
        return base;
    } else {
        return base * power(base,(exp - 1))
    }
}

function factorial(num) {
    if (num <= 2) {
        return num;
    } else {
        return num * factorial(num - 1);
    }
}

function productOfArray(arr) {

    function helper(helperArr) {
       console.log(helperArr); 
       if (helperArr.length == 2) {
           return helperArr[0] * helperArr[1];
       } else {
           helperArr[1] = helperArr[0] * helperArr[1];
           return helper(helperArr.slice(1))
        }
    }

    return helper(arr);
}

function recursiveRange(num) {
    if (num <= 0) {
        return num;
    } else {
        return num + recursiveRange(num - 1);
    }
}

function fib(n){
    if (n <= 2) return 1;
    return fib(n-1) + fib(n-2);
}

// console.log(power(2,0));
// console.log(power(2,4));
// console.log(power(2,2));

// console.log(factorial(1)) // 1
// console.log(factorial(2)) // 2
// console.log(factorial(4)) // 24
// console.log(factorial(7)) // 5040

// console.log(productOfArray([1,2,3])) // 6
// console.log(productOfArray([1,2,3,10])) // 60)

// console.log(recursiveRange(6)) // 21
// console.log(recursiveRange(10)) // 55 

console.log(fib(4))// 3
console.log(fib(10) )// 55
console.log(fib(28) )// 317811
console.log(fib(35) )// 9227465