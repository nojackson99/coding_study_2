//plan
//use rest parameter to store arguments in array
//use multiple pointers to check if there are duplicates

function areThereDuplicates(...arr)
{
    let str1Freq = {}

    //checking frequency of item
    for (let i = 0; i < arr.length; i++) {
        const element = arr[i];  
        if (str1Freq[`${element}`] == null) {
            str1Freq[`${element}`] = 1;
        }
        else {
            return true
        }
         
    }

    return false;
}

let duplicatesExist = areThereDuplicates('a','b','c','a');
console.log(duplicatesExist);