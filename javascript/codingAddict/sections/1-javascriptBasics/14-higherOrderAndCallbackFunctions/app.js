// Callback and higher order functions
// Functions can be stored in varaibles and both passed to and returned from other functions
// higher orderfuntions - accept functions as agrument or pass functions as result
// callback - functions that has been passed to another function and called within

function morning(yourName) {
    return `Good morning ${yourName.toUpperCase()}`;
}

function afternoon(yourName) {
    return `Good afternoon ${yourName.repeat(3)}`;
}

function evening(yourName) {
    return `Good evening ${yourName.toUpperCase()}`;
}

// here we have timeOfDay receiving function argument timeOfDay
// greet is considered a higher order function because it is accepting another function as a parameter
function greet(yourName, timeOfDay){
    myName = `Noah`;
    
    // functions can be called with $interpolation
    console.log(`${timeOfDay(yourName)}, my name is ${myName}.`);
}

yourName = `Alyssa`;

greet(yourName, morning);
greet(`Ami`, afternoon);
greet('Andrew', evening);
