class Node{
    constructor(val){
        this.val = val;
        this.next = null;
    }
}

class SinglyLinkedList{
    constructor(){
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    push(val){
        let newNode = new Node(val);
        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }
        this.length++;
        return this;
    }
    pop(){
        if (!this.head) {
            return undefined;
        }

        var current = this.head;
        let prev = current
        
        while (current.next) {
            prev = current;
            current = current.next
        }

        this.tail = prev
        this.tail.next = null;
        this.length--;
        if(this.length === 0){
            this.head = null;
            this.tail = null;
        }
        return current

    }
    shift(){
        if (!this.head) return undefined;
        var oldHead = this.head;
        this.head = oldHead.next;
        this.length--;
        return oldHead;
    }
    unshift(val){
        let newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            newNode.next = this.head
            this.head = newNode
        }

        this.length++;
        return this;
    }
    get(index){
        if (index >= 0 && index < this.length) {
            let current = this.head;
            for (let i = 0; i < index; i++) current = current.next;
            return current;
        } else return undefined;
    }
    set(index,val){
        let foundNode = this.get(index);
        if(!foundNode) return false;
        else {
            foundNode.val = val;
            return true;
        }
    }
    insert(index, value){
        // attempting to insert node at invalid index
        if (index < 0 || index > this.length) return false
        // inserting element at the end of the list
        else if (index === this.length) return !!this.push(value)
        // inserting element at the beginning of the list
        else if (index === 0) return !!this.unshift(value)
        // inserting element at index other than end or beginning
        else {
            let newCurr = new Node(value);
            let current = this.get(index - 1);
            newCurr.next = current.next;
            current.next = newCurr;
            this.length++;
            return true;
        }      
    }
    remove(index){
        // attempting to remove node at invalid index
        if (index < 0 || index >= this.length) return undefined
        // removing element at the end of the list
        else if (index === this.length - 1) return this.pop()
        // removing element at the beginning of the list
        else if (index === 0) return this.shift()
        // removing element at index other than end or beginning
        else {
            let current = this.get(index - 1);
            let removed = current.next
            current.next = removed.next;
            this.length--;
            return removed;
        }

    }
    reverse(){
        let node = this.head;
        this.head = this.tail;
        this.tail = node;

        let next = null
        let prev = null

        for (let i = 1; i <= this.length; i++) {
            next = node.next
            node.next = prev
            prev = node
            node = next;
        }

        return this;
    }
} 

let myList = new SinglyLinkedList();

// console.log(myList);

myList.push(13);
myList.push(27);
myList.push(32);
myList.push(71);
myList.push(96);
myList.reverse();

console.log(myList);


