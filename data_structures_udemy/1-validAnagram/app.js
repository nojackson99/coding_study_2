console.log("hello world");

//Plan
//create test cases
//create function to test the anagram
//if length does not match return false
//count frequency of letters in string one and store in object
//count frequency of letters in string two and store in object
//check frequency of elements in each object
//if frequency mathces return true
//else return false


function validAnagram(string1, string2)
{
    if (string1.length !== string2.length)
    {
        console.log(`strings are not the same length`);
        return false
    }

    let str1Freq = {}

    let str2Freq = {}

    //checking frequency of string 1
    for (let i = 0; i < string1.length; i++) {
        const element = string1[i];        
        if (str1Freq[`${element}`] == null) {
            str1Freq[`${element}`] = 1;
        }
        else {
            str1Freq[`${element}`] += 1;
        }
         
    }
    
    //check frequency of string 2
    for (let i = 0; i < string2.length; i++) {
        const element = string2[i];        
        if (str2Freq[`${element}`] == null) {
            str2Freq[`${element}`] = 1;
        }
        else {
            str2Freq[`${element}`] += 1;
        }
         
    }

    //check if str1Freq and str2Freq are the same
    for (const [key, value] of Object.entries(str1Freq)) {
        tempValue = str2Freq[`${key}`];
        if (tempValue !== value) {
            return false;
        }
      }

    return true;
    


    // console.log(str1Freq);
    // console.log(str1Freq);
}

let isValid = validAnagram(`awesome`,`fefef`);
console.log(isValid);