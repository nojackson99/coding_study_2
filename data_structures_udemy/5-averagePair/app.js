//plan
//create aveargePair function
//create index variables
//set 1st index to first element and 2nd to last
//get averag of first and second indexes
//if average is too small increment first
//if average is too large increment second
//while loop they are not the same

function averagePair(arr,avg) {
    let first = arr[0];
    let second = arr[arr.length - 1];

    if (arr.length == 0) {
        return false
    }

    while (first !== second) {
        tempAvg = (arr[first] + arr[second]) / 2
        if (tempAvg == avg) {
            return true;
        } else if (tempAvg < avg) {
            first++;
        } else {
            second--;
        }
    }

    return false;
}

const array = [];
const average = 4;

let averageExists = averagePair(array,average);
console.log(averageExists);