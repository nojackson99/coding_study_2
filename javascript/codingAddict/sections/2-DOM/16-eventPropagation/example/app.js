//event propagation example

const container = document.querySelector('.container')
const btn = document.querySelector('.btn')
const heading = document.querySelector('.heading')

//using event bubbling to select dynamic heading
function sayHello(event){
    if (event.target.classList.contains('heading')) {
        console.log('hello there');

    }
}

function addElement(){
    const element = document.createElement('h1');
    element.classList.add('heading');
    element.textContent = `i'm inside the container`;
    container.appendChild(element)
};

container.addEventListener('click', sayHello);

btn.addEventListener('click',addElement)