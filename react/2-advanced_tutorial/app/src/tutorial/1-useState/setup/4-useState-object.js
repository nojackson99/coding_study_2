import React, { useState } from 'react';


const UseStateObject = () => {
  const [person, setPerson] = React.useState(
    {
      name: 'peter',
      age: 24,
      message: 'random message',
    }
  );
  // when using useState on an object
  // invoking setPerson('hello world') would change pesron from
  // an object to the string 'hello world' to prevent this we use
  // spread operator which copies the person object and allows use to modify specific values
  const changeMessage = ()=> {
    setPerson({...person, message: 'hello world'})
  };
  console.log(person);
  return( 
  <>
    <h3>{person.name}</h3>
    <h3>{person.age}</h3>
    <h3>{person.message}</h3>
    <button onClick = {changeMessage}>change message</button>
  </>
  );
};

export default UseStateObject;
