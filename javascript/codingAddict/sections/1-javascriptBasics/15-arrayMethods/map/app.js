// Array methods

// map
// returns a new array
// does not change size of original array
// uses values from original array when making new one
// performs code in call back function on each element of array

// array of people objects
const people = [
    {name: 'Noah',age: 22,job: 'developer'},
    {name: 'Alyssa',age: 22,job: 'student'},
    {name: 'Ami',age: 1,job: 'dog'}
];

//will store new array returned by people.map
const ageinFiveYears = people.map(function(people) {
    return people.age + 5;
});

const newPeople = people.map(function(person){
    return {
        firstName: person.name.toUpperCase(),
        oldAge: person.age + 20,
    }
})

const names = people.map(function(person) {
    return `<h1>${person.name}</h1>`;
});

document.body.innerHTML = names.join('');

console.log(newPeople);
console.log(ageinFiveYears);
console.log(names);