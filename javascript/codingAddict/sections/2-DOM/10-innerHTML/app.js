// innerHTML vs textContent

const list = document.getElementById('first')
const div = document.getElementById('second')
const item = document.querySelector('.item');

// console.log(div.textContent);
// console.log(list.innerHTML);

// .innerHTML faster way of dynamically adding html using javascript
const ul = document.createElement('ul');

// since backticks are used here we can use interpolation ${} to insert text
// into this expression
ul.innerHTML = `<li class="item">list item</li>
                <li>list item</li>`

document.body.appendChild(ul);