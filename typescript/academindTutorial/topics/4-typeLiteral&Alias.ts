//type alias: create new type that can replace union or literal types
type numStr = number | string;
type result = 'number' | 'text';

function combine(
    //below we see aliases used to define types for input1, input2, and resultType
    input1: numStr, 
    input2: numStr, 
    resultType: result            //here we define our data type as the data we expect for this variable
    )                             //instead of string as type we have 'number' or 'text' because those are the only possiblities
{
    let result;
    if (typeof input1 === 'number' && typeof input2 === 'number') 
    {
        result = input1 + input2;
    } else 
    {
        result = input1.toString() + input2.toString();
    }
    
    //this ifelse statement checks what type we want result to be output as
    if (resultType === 'number')
    {
        return +result;
    } else 
    {
        return result.toString();
    }
};

const combinedAges = combine(30, 26, 'number');
console.log(combinedAges);

const combinedStringAges = combine('30', '26', 'number');
console.log(combinedStringAges);

const combinedNames = combine('Max', 'Anna', 'text');
console.log(combinedNames);

