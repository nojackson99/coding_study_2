function divideArrays(arr) {
    //base case
    if(arr.length <= 1) return arr;

    //recursive case
    let mid = Math.floor(arr.length/2);
    let left = divideArrays(arr.slice(0,mid));
    let right = divideArrays(arr.slice(mid));

}