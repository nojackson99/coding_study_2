// ES6 Classes

class Account {
    constructor(name,initialBalance){
        this.name = name;
        this.balance = initialBalance;
    }
    // no let/var/const keyword needed for defining class property
    bank = 'Huntington'
    deposit(amount){
        this.balance += amount;
        console.log(`Hello ${this.name}, your balance is ${this.balance}`);
    }
}

const noah = new Account(`noah`, 0);
console.log(noah);
console.log(noah.name);
noah.deposit(500);

const alyssa = new Account(`alyssa`, 200);
console.log(alyssa);
console.log(alyssa.name);
alyssa.deposit(700);