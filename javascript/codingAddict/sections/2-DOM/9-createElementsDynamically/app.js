// createElementsDynamically

// create element
const result = document.querySelector('#result');

// create empty element
const bodyDiv = document.createElement('div');

// create text node
const text = document.createTextNode('a simple body div');

//we append text to the created div
bodyDiv.appendChild(text);

//then append this div to document body
document.body.appendChild(bodyDiv);

//inserts bodyDiv before result
//this will replace the bodyDiv append above
document.body.insertBefore(bodyDiv, result);



// console.log(result.children);

const heading = document.createElement('h2');
const headingText = document.createTextNode('dynamic h2 heading');

heading.appendChild(headingText);
heading.classList.add('blue');
// result.appendChild(heading);

//inserts heading before first element
const first = document.querySelector('.red');
result.insertBefore(heading, first);

//replaceChild
const heading6 = document.createElement('h6')
const heading6Text = document.createTextNode('I am an h6 element');
heading6.appendChild(heading6Text);
heading6.classList.add('red');

//replace second param with first param
document.body.replaceChild(heading6, bodyDiv);
console.log(heading6);

