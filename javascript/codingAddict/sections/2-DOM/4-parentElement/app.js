// parentElement

const heading = document.querySelector('h2');

//.parentElement can be chained to move up dom element tree
// eventually we will reach null once we reach past the top of the tree
console.log(heading.parentElement);

const parent = heading.parentElement;

//applies styling to entire div with id='second'
parent.style.color = 'red'

