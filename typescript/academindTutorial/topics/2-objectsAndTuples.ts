const person0: {                         // const person is an object which is a way to group variables with types defined by the coder.
    name: string;                       // in both javascript and typescript the types are inferred by the launguage.
    age: number;                        // types can be defined and overriden with the with the code shown here.
    hobbies: string[];                  // best practice is to not define types unless an override is needed such as overriding type
    role: [number, string];             // from array to tuple. A Tuple is a special array type with x number of elements with type of
} = {                                   // each element defined by the coder.
    name: 'Noah',                       // In this example we have the tuple person.role which has a number element and a string element
    age: 22,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};

// .push method is an exception to tuple rules and can lead to tuple with more than 2 elements?
// person.role.push('admin');

// results in error because the second element in person.role is defined as a string so 10 can not be assigned to it
// person.role[1] = 10; 

// results in error because we cannot manually assign three elements to property role because role has already been defined as a tuple 
// person.role = [0, 'admin', 'user'];

// favoriteActivities defined here as a string array
let favoriteActivities: string[];
favoriteActivities = ['Sports'];

// For loop to console log all data in person.hobbies
for (const hobby of person.hobbies) {
    console.log(hobby);    
}