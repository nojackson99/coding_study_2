* when toggle button is clicked menu either shows or hides

start main {
    //variables
    set menu toggle element

    start toggleMenu {
        if list class list contains show-links {
            remove show-links class
        }
        else {
            add show-links class
        }
    }

    add menu toggle event listener (click, calls toggleMenu)

}