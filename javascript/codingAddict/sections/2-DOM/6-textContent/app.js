// textContent
// nodeValue

const item = document.getElementById('last');
console.log(item);

//two ways for returning text of slected element
console.log(item.childNodes[0].nodeValue)
console.log(item.firstChild.nodeValue);

//does the same as using nodeValue
//simpler process
const easyValue = item.textContent;
console.log(easyValue);


