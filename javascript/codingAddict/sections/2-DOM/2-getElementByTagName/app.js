// get elements by tag name

// headings will now be a node list which is similar to array
// keep in mind array methods do not easily work on node lists
const headings = document.getElementsByTagName('h2');
console.log(headings);

//using headings node list to change an elements color
headings[0].style.color = 'red';

console.log(headings.length);

const listItems = document.getElementsByTagName('li');

//this results in error
// listItems.forEach(function(item){
//     console.log(item);
// })

//can use for loop instead OR 
//using ... operator
const betterItems = [...listItems];

betterItems.forEach(function(item) { 
    console.log(item);
})

console.log(listItems);
console.log(betterItems);