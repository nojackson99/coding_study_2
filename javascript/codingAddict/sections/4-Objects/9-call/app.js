// call

const john = {
    name: 'john',
    age:24,
    greet : function(){
        console.log(this);
        console.log(`Hello, I'm ${this.name} and I'm ${this.age} years old`)
    },
};

const susan = {
    name: 'susan',
    age: 21,
};

john.greet();

function greet() {
    console.log(this);
    console.log(`Hello, I'm ${this.name} and I'm ${this.age} years old`)
}

greet.call(john);

// allows for calling greet on an object passed in
// call designates the object as this when executing greet() code
greet.call(susan);

// also works for newly defined objects
greet.call({name: 'peter', age:30})

