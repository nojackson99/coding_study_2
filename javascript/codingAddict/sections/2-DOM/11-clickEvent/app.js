// clickEvents

// core princables
//   1. select element
//   2. addEventListener()
//   3. what event, what to do

const myBtn = document.querySelector(".btn")

const eventsHeading = document.querySelector('h2')
console.log(eventsHeading);

//1st param is type of event
//2nd param is callback funciton with code that will execute upon event trigger
//  can be formally declared or annonymous
myBtn.addEventListener('click',function(){
    console.log(myBtn);
    eventsHeading.classList.add('red');
});