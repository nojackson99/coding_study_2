// ==========================================================================
// MERGE SORT
// ==========================================================================
function merge(arr1,arr2) {
    let result = [];
    let i = 0;
    let j = 0;

    while ((i < arr1.length) && (j < arr2.length)) {
        //check which array has smallest elementa and push to result
        if (arr1[i] < arr2[j]) {
            result.push(arr1[i])
            i++;
        } else {
            result.push(arr2[j])
            j++;
        }
        //push rest of other array once one array has all elements pushed to result
        if (i === arr1.length) {
            for (let k = j; k < arr2.length; k++) {
                result.push(arr2[k])
            }
        }
        if (j === arr2.length) {
            for (let k = i; k < arr1.length; k++) {
                result.push(arr1[k])      
            }
        }
    }
    //edge case: if one array empty return non empty array
    if (arr1.length === 0) return arr2
    else if (arr2.length === 0) return arr1

    //returns result once arrays have been merged
    else return result
}

function mergeSort(arr) {
    //base case
    if(arr.length <= 1) return arr;

    //recursive case
    let mid = Math.floor(arr.length/2);
    let left = mergeSort(arr.slice(0,mid));
    let right = mergeSort(arr.slice(mid));

    return merge(left,right)
}


// ==========================================================================
// INSERTION SORT
// ==========================================================================
function insertionSort(arr) {
    for (let i = 1; i < arr.length; i++) {
        for (let j = i; j > 0; j--) {
            if (arr[j] < arr[j - 1]) {
                let temp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = temp
            } else break;
        }      
    }
    return arr;
}


// ==========================================================================
// SELECTION SORT
// ==========================================================================
function selectionSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        let min = i
        for (let j = i; j < arr.length; j++) {
            if (arr[j] < arr[min]) {
                min = j
            }   
        }
        if (min !== i) {
            let temp = arr[i]
            arr[i] = arr[min];
            arr[min] = temp;
        }
    }

    return arr;
}


// ==========================================================================
// BUBBLE SORT
// ==========================================================================
function bubbleSort(arr) {
    let swapMade = null

    for (let i = arr.length - 1; i > 0; i--) {
        swapMade = false;
        for (let j = 0; j < i - 1; j++) {
            if (arr[j] > arr[j+1]) {
                let temp = arr[j];
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
                swapMade = true;
            }
        }
        if (!swapMade) {
            break;
        }
    }
    return arr;
}


myArray = [1502, 797, 612, 1251, 1876, 195, 1249, 744, 499, 1560, 840, 1995, 425, 483, 1582, 1341, 1437, 380, 1548, 1151, 237, 389, 760, 1743, 848, 
           1100, 381, 1580, 1283, 1623, 453, 1318, 878, 1257, 435, 529, 1094, 192, 963, 330, 1332, 852, 1399, 242, 386, 541, 934, 1516, 1861, 1853, 
           607, 468, 696, 54, 613, 369, 112, 912, 1578, 785, 560, 403, 1612, 1847, 297, 596, 17, 188, 866, 282, 470, 1573, 1155, 1708, 1067, 1992, 
           1988, 615, 50, 545, 1186, 1591, 540, 1712, 939, 1304, 1949, 1375, 1006, 197, 608, 1130, 1528, 1827, 1410, 870, 1645, 352, 1344, 520, 1314, 
           1635, 1797, 1646, 1753, 1003, 896, 645, 1047, 373, 1630, 10, 5, 1559, 1125, 388, 1270, 205, 1317, 506, 1307, 892, 921, 1462, 741, 926, 241, 
           1198, 1567, 1218, 1772, 430, 1639, 1961, 1902, 1412, 908, 1783, 412, 1784, 1752, 1987, 244, 899, 953, 183, 1464, 1353, 765, 1028, 1763, 269, 
           1163, 35, 85, 326, 684, 37, 1534, 1426, 1545, 25, 907, 941, 922, 1633, 418, 883, 831, 1729, 966, 1777, 1965, 149, 914, 739, 310, 1471, 1696, 
           533, 1343, 213, 672, 676, 683, 1880, 9, 292, 827, 1429, 1741, 856, 1308, 1279, 1781, 300, 1085, 1780, 936, 543, 1255, 1455, 1971, 834, 1519, 
           681, 710, 841, 1074, 734, 1833, 805, 1191, 724, 1265, 782, 1655, 1228, 76, 420, 938, 281, 570, 1607, 152, 39, 1663, 392, 444, 832, 1841, 829, 
           1454, 1495, 1029, 1716, 1340, 1368, 1398, 199, 1485, 1439, 1883, 847, 1468, 1290, 689, 1687, 784, 139, 218, 1300, 1802, 1860, 1038, 1854, 749, 
           1159, 309, 775, 1451, 1216, 1153, 1747, 951, 1872, 1053, 1483, 1423, 1806, 738, 1826, 1738, 1675, 1021, 1164, 143, 272, 910, 783, 1322, 235, 564, 
           1473, 819, 1925, 114, 1855, 1062, 1215, 1916, 344, 851, 1748, 1980, 1233, 323, 1090, 1273, 634, 276, 822, 902, 1893, 140, 617, 1933, 597, 1650, 
           1517, 146, 621, 1831, 942, 974, 1970, 1313, 1364, 475, 1613, 1521, 955, 1962, 1194, 1373, 1595, 1724, 414, 416, 1064, 1810, 791, 947, 1487, 
           1027, 824, 691, 1868, 1475, 1248, 1908, 1890, 472, 591, 41, 1714, 136, 262, 1536, 1773, 485, 1362, 113, 1803, 1349, 456, 1002, 1698, 1553, 133, 
           1453, 79, 1171, 1535, 1112, 583, 682, 1804, 1717, 999, 647, 1185, 964, 7, 264, 954, 585, 820, 1132, 509, 366, 423, 1572, 1258, 1379, 1662, 1070, 
           400, 1355, 1226, 1629, 316, 906, 1609, 638, 1345, 1259, 842, 106, 1837, 737, 633, 960, 267, 459, 1538, 1725, 187, 1652, 228, 452, 725, 1818, 1294, 
           502, 306, 348, 1101, 813, 688, 1324, 1809, 181, 1311, 665, 58, 1342, 1782, 155, 780, 268, 1913, 1128, 709, 1035, 148, 551, 1800, 1394, 336, 643, 
           1581, 1920, 1158, 1801, 275, 1240, 1309, 1692, 806, 1505, 1407, 1575, 1354, 1701, 1683, 1611, 1642, 632, 357, 1774, 1383, 539, 325, 1794, 254, 1917, 
           404, 616, 949, 324, 920, 1004, 1524, 618, 1264, 868, 1424, 23, 1396, 659, 1146, 97, 1510, 1041, 733, 365, 1721, 1570, 861, 1951, 449, 721, 134, 678, 
           1134, 440, 698, 1842, 1939, 527, 900, 1745, 1020, 1180, 1859, 372, 971, 96, 1761, 1937, 434, 1210, 563, 1346, 876, 1907, 1302, 790, 1269, 754, 1244, 
           1476, 246, 1593, 880, 1824, 962, 1641, 159, 1397, 1474, 1884, 644, 374, 307, 580, 1617, 1754, 1684, 81, 859, 1976, 383, 1843, 1844, 175, 1648, 855, 
           1030, 990, 1511, 1492, 1590, 349, 723, 595, 1337, 1033, 1092, 409, 123, 1320, 1181, 105, 998, 789, 729, 561, 253, 1390, 1660, 1715, 1989, 1857, 1234, 
           804, 1735, 226, 147, 318, 1566, 129, 1442, 31, 1298, 677, 1110, 661, 1415, 568, 1411, 528, 401, 973, 1695, 836, 8, 969, 1333, 1728, 361, 1096, 1452, 
           1734, 65, 1900, 671, 526, 1285, 158, 224, 1892, 1914, 1310, 1600, 1400, 1054, 862, 757, 1659, 301, 1846, 1604, 1691, 786, 410, 411, 1834, 303, 1983, 
           176, 1947, 339, 701, 481, 36, 1901, 648, 1740, 511, 1376, 1097, 1408, 752, 406, 1076, 546, 1209, 587, 1192, 1811, 610, 1363, 1533, 957, 1281, 1370, 
           59, 692, 247, 1547, 1252, 927, 1043, 1910, 1277, 1431, 1807, 1169, 461, 1274, 1106, 315, 1865, 1050, 1271, 1795, 182, 61, 755, 956, 1263, 1610, 379, 
           977, 599, 1166, 1867, 1235, 695, 690, 961, 1272, 364, 1990, 1537, 588, 1792, 845, 1480, 1422, 320, 720, 512, 151, 1887, 441, 1850, 26, 1402, 928, 
           978, 839, 377, 756, 1885, 279, 109, 1981, 1679, 894, 522, 933, 1838, 1115, 93, 1644, 1921, 1697, 1241, 1681, 566, 34, 413, 417, 1541, 1520, 397, 727, 
           1832, 1141, 1531, 796, 1023, 1348, 1039, 431, 200, 20, 1268, 1509, 886, 1187, 1445, 86, 1899, 916, 1037, 270, 703, 536, 1757, 630, 1554, 439, 1584, 
           408, 628, 776, 1628, 1839, 98, 1943, 1799, 239, 1978, 686, 704, 1224, 161, 1490, 71, 1111, 1602, 1222, 1722, 422, 521, 214, 1262, 1282, 73, 1229, 
           1869, 332, 173, 1596, 1072, 1367, 122, 1830, 398, 1015, 1223, 1823, 538, 1413, 480, 1539, 798, 1149, 1750, 1060, 1996, 179, 432, 897, 56, 980, 1544, 
           929, 716, 1766, 1167, 1200, 1126, 426, 825, 542, 1055, 1605, 1102, 1968, 1330, 1225, 1967, 68, 252, 209, 333, 1564, 1552, 1764, 1493, 1586, 1266, 
           1532, 833, 1202, 1045, 1204, 189, 1819, 500, 1731, 497, 1479, 1107, 1703, 1138, 359, 378, 345, 1526, 1467, 1791, 715, 131, 1416, 1760, 4, 664, 903, 
           127, 995, 1174, 1036, 1549, 1077, 193, 1616, 885, 700, 427, 531, 514, 1985, 792, 1327, 1658, 1935, 1982, 395, 940, 322, 64, 1297, 989, 1958, 1193, 802, 
           216, 968, 1776, 965, 556, 602, 203, 985, 172, 415, 1105, 1848, 1676, 1744, 1585, 1098, 1057, 1246, 1254, 1592, 590, 1936, 190, 1999, 1812, 864, 1894, 
           945, 63, 1421, 1335, 355, 1489, 1972, 850, 1434, 742, 1178, 1481, 28, 707, 1173, 895, 89, 462, 1211, 1654, 849, 1670, 1419, 256, 120, 815, 800, 915, 
           1836, 1034, 223, 271, 1261, 100, 295, 1393, 843, 145, 1014, 117, 135, 351, 1005, 1788, 869, 635, 518, 178, 1963, 972, 1165, 837, 150, 717, 1315, 603, 
           1577, 1484, 708, 1959, 342, 808, 1568, 1500, 501, 1820, 1956, 474, 984, 817, 1359]


let t0 = performance.now();
mergeSort(myArray);
let t1 = performance.now();
console.log(`Call to mergeSort took ${t1 - t0} milliseconds.`);

t0 = performance.now();
bubbleSort(myArray);
t1 = performance.now();
console.log(`Call to bubbleSort took ${t1 - t0} milliseconds.`);

t0 = performance.now();
console.log(insertionSort(myArray));
t1 = performance.now();
console.log(`Call to insertionSort took ${t1 - t0} milliseconds.`);

t0 = performance.now();
selectionSort(myArray);
t1 = performance.now();
console.log(`Call to selectionSort took ${t1 - t0} milliseconds.`);