//refactor
//change so that there is only 1 frequency object and the second for loop will reduce element counter
//as it comes across the element in the second item
//if counter is zero or null this means the two items do not match in frequency so false can be returned
//this allows for removal of third for loop that checks for matching between object 1 and 2

//Plan



function validAnagram(item1, item2)
{
    if (item1.length !== item2.length)
    {
        console.log(`items are not the same length`);
        return false
    }

    let str1Freq = {}

    //checking frequency of item 1
    for (let i = 0; i < item1.length; i++) {
        const element = item1[i];  
        if (str1Freq[`${element}`] == null) {
            str1Freq[`${element}`] = 1;
        }
        else {
            str1Freq[`${element}`] += 1;
        }
         
    }
    
    //iterate through item 2 subtracting fequency of each element
    for (let i = 0; i < item2.length; i++) {
        const element = item2[i];
        
        //element found in item 2 that is not present in item 1
        if (str1Freq[`${element}`] == null) {
            return false;
        }
        //check if frequency of element is not mathcing
        else if (str1Freq[`${element}`] == 0) {
            return false
        }
        //update frequency of item 1 object
        else {
            str1Freq[`${element}`] -= 1;
        }
    }

    return true;
}

function convertToArray(item) {
    let newArray = Array.from(String(item), Number);
    return newArray;
}


const n1 = convertToArray(3589578);
const n2 = convertToArray(5879385);

let isValid = validAnagram(n1,n2);
console.log(isValid);