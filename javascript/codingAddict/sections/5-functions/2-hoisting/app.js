// Hoisting
// functions and var declarations (but not var values) are moved to top of program on execution
// however best practice is to only invoke functions after they are initialized

const firstName = 'john';
let lastName = 'jordan';
var random = 'random';

function display() {
    console.log('hello world');
}