// Global scope vs Local Scope

// variables outside of {} are in global scope
// watch out for accidental name collisions with variables and accidental modifications

//these variables are declared globally and can be accessed by functions
let name = 'noah';
name = 'alyssa';

function func(){
    console.log(name);
    
    //here we can see that name declared in main is accessed and changed in func
    name = 'andrew'
    console.log(name);

    const age = 22;

    //declaring variable without keyword will declare it in global scope
    globalVar = 'globalVar'
}

func();

//this will result in error because age is only declared locally in func
//console.log(age);

//globalVar can be console logged because it was declared globally in func
console.log(globalVar);

