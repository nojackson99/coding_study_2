// String properties and methods

let text = ' Noah Jackson';

//get length of string (includes whitespaces)
let len = text.length;
console.log(len);

//string method can also be logged directly
console.log(text.length);

//change string to lower or upper case
console.log(text.toLowerCase());
console.log(text.toUpperCase());

//string is 0 indexed array
//charAt finds character at specified element
console.log(text.charAt(1));

console.log(text.charAt(text.length - 1))

//finds first elemnt that matches string
//returns -1 if not found
console.log(text.indexOf('o'));

//removes whitespace at each end
console.log(text.trim());

//methods can be chained together to perform multipe actions to a string
console.log(text.trim().toLowerCase().startsWith('noah'))

//looks for specified string in target string
console.log(text.includes('ack'))

//in this example returns text elements 0-3
console.log(text.slice(0,4));

//can also grab elemnts at the end of the string
console.log(text.slice(-3));