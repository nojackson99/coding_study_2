// Array methods

const numbers = [0, 1, 2, 3, 4, 5];

// show all numbers
for (let index = 0; index < numbers.length; index++) {
    console.log(numbers[index]);
}

// forEach
// does not return new array
// performs code in call back function on each element of array

// array of people objects
const people = [
    {name: 'Noah',age: 22,job: 'developer'},
    {name: 'Alyssa',age: 22,job: 'student'},
    {name: 'Ami',age: 1,job: 'dog'}
]

// callback function that is passed to forEach method
function showPerson(person){
    console.log(person.job.toUpperCase());
}

// require callback function to dictate actions performed on each array element
people.forEach(showPerson);

// same as above code but callback funciton is declared as annonymous within forEach arguments
people.forEach(function(person){
    console.log(person.job.toUpperCase());
})