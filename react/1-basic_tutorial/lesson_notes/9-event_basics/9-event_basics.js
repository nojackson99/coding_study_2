// 9-event_basics

import React from 'react';
import ReactDom from 'react-dom';

// css
import "./index.css";

// variables
const books = [
  {
    id: 1,
    img:
      'https://images-na.ssl-images-amazon.com/images/I/81a5KHEkwQL._AC_UL604_SR604,400_.jpg',
    title: `Oh, the Places You'll Go!`,
    author: 'Dr. Seuss'
  },
  {
    id: 2,
    img:
      'https://images-na.ssl-images-amazon.com/images/I/81EVdWdmOKL._AC_UL604_SR604,400_.jpg',
    title: 'Brown Bear, Brown Bear, What Do You See?',
    author: 'Bill Martin Jr / Eric Carle'
  },
  {
    id: 3,
    img:
      'https://images-na.ssl-images-amazon.com/images/I/91cqEsyjd-L._AC_UL604_SR604,400_.jpg',
    title: 'The Very Hungry Catepillar',
    author: 'Eric Carle'
  }
]

function BookList() {
  return (
    <section className='booklist'>
      {books.map((book) => {
        return <Book key={book.id} {...book}></Book>
      })}
    </section>
  );
}

const Book = ({ img, title, author }) => {
  // creating event listener by reference
  // here we create a clickHandler function that defines what happens when event is triggered
  // we reference with <button type = "button" onClick={clickHandler}>example</button>
  const clickHandler = () => {
  alert('hello world');
  }
  // when passing in data dynamically to an event handler for a reference event listener
  // use an arrow function
  const complexExample = () => {
    console.log(author);
  }
  return (
    <article className='book'>
      <img src={img} alt="" />
      {/* in-line example of event listener */}
      <h2 onClick={() => alert(title)}>{title}</h2>
      <h4>{author}</h4>
      {/* simple event listener by reference */}
      <button type="button" onClick={clickHandler}>
        reference example
      </button>
      {/* event listner by reference with passed in data */}
      {/* here we use an arrow function, "complexExmaple(author) by itself 
      would cuase the function to be invoked on page load" */}
      <button type="button" onClick={() => complexExample(author)}>
        more complex example
      </button>
    </article>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));