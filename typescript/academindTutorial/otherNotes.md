Watch Mode:
-This tells typescript to watch a file and automatically compile when changes are made
-Right now after each change is made I must save then execule tsc app.ts
-By doing tsc app.ts --watch OR -w I can enter watch mode
-watch mode can only work on one file at a time which makes it less effective when multiple
 files are being worked on

Tsc iniit:
-running command tsc --init tells vsc to initiate typescript project
-will add tsconfig.json with different typescript settings
-allows for using the tsc command to compile all tsc files in the folder
-make sure to run command in the folder where tsc files are, tsc will
 not compile files in other folders
-combined with -w ex: tsc -w to initiate watch mode on all files

 