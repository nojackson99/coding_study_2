// classList - shows/gets all classes
// contains - checks classList for specific class
// add - add class
// remove - remove class
// toggle - toggles class

//set html elements to variables
navToggle = document.querySelector(".nav-toggle");
menuLinks = document.querySelector(".links");

//on nav toggle click hides menu if shown and shows menu if hidden
function toggleMenu() {
    if (menuLinks.classList.contains("show-links")) {
        menuLinks.classList.remove("show-links")
    }
    else {
        menuLinks.classList.add("show-links")
    }
}

//alternative
function toggleMenu2() {
    menuLinks.classList.toggle("show-links");
}

//event listner for nav toggle
navToggle.addEventListener("click",toggleMenu)