// 9-import_and_export
export const books = [
    {
      id: 1,
      img:
        'https://images-na.ssl-images-amazon.com/images/I/81a5KHEkwQL._AC_UL604_SR604,400_.jpg',
      title: `Oh, the Places You'll Go!`,
      author: 'Dr. Seuss'
    },
    {
      id: 2,
      img:
        'https://images-na.ssl-images-amazon.com/images/I/81EVdWdmOKL._AC_UL604_SR604,400_.jpg',
      title: 'Brown Bear, Brown Bear, What Do You See?',
      author: 'Bill Martin Jr / Eric Carle'
    },
    {
      id: 3,
      img:
        'https://images-na.ssl-images-amazon.com/images/I/91cqEsyjd-L._AC_UL604_SR604,400_.jpg',
      title: 'The Very Hungry Catepillar',
      author: 'Eric Carle'
    }
  ]