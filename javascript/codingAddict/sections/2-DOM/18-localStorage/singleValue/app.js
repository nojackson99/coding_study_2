// local storage

//1st param is key 2nd param is value
//using same key twice will override
localStorage.setItem('name', 'Noah');
localStorage.setItem('friend', 'Andrew');
localStorage.setItem('job', 'developer');
localStorage.setItem('address', '308 Campbell Hill Rd.');
sessionStorage.setItem('name', 'john');


const valueFromLocal = localStorage.getItem('name')
console.log(valueFromLocal);

localStorage.removeItem('name');

//clears local storage
localStorage.clear();