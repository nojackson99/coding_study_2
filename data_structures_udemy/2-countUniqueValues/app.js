console.log("hello world");

//Plan
//1. create index variables set to first two positions in array
//2. compare indexes if same increment second counter if different increment first counter and set element
//   equal to element at second index then increment second counter
//3. continue this until second counter reaches end of array
//4. use first index to report number of unique values

function countUniqueValues(arr) {
    let i = 0;
    let j = 1;

    while (j < arr.length) {
        if (arr[i] === arr[j]) {
            j++;
        } else {
            i++;
            arr[i] = arr[j];
            j++;
        }
    }

    if (arr.length == 0)
    {
        return 0;
    }

    return(i + 1);
}

let values = [1,1,2,3,3,4,5,6,6,7];

console.log(countUniqueValues(values));
