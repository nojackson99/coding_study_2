* Objectives
    - screen shows counter value
        - when positive green, zero black, negative red
        - Increase and decrease buttons increment and decrement counter
        - counter can be reset to zero with button

* Variables
    - decrease button html elemnt
    - increase button html element
    - reset button html element
    - counter value html element
    - temp number variable

* Functions
    - increase counter
    - reset counter
    - decrease counter
    - button click events



start main{
    
    // variables
    store buttons in an array
    store counter value display

    start checkCounter {
        if button text is increase {
            increment counter value
        } else if button text is decrease {
            decrement counter value
        } else {
            set counter to zero
        }

        if counter is positive {
            set color to green
        } else if counter is negative {
            set counter color to red
        } else {
            set counter color to black
        }
    }

    buttons array forEach(annonymous) {
        set click event listener 
    }



}


* psudocode for using switch and button parameters

* Variables
    - decrease button html elemnt
    - increase button html element
    - reset button html element
    - counter value html element
    - temp number variable

* Functions
    - change counter

start main{
    
    // variables
    store decrease button
    store reset button
    store increase button
    store counter value display

    start check counter(event) {
        store counter value text
        typecast counter value to int

        start button flag check {
            case decrease:
                decrement counter value
                if counter value is negative {
                    set counter value display color to red }
            case increase:
                increment counter value
                if counter value is positive {
                    set counter value display color to green }
            default:
                set counter value to zero
                set counter value display color to black
        }

        update counter value display text
    }


    decrease click event handler
    set decrease param flag

    reset click event handler
    set reset param flag

    increase click event handler
    set increase param flag



}