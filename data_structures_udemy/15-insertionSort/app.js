function insertionSort(arr) {
    for (let i = 1; i < arr.length; i++) {
        for (let j = i; j > 0; j--) {
            if (arr[j] < arr[j - 1]) {
                let temp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = temp
            } else break;
        }      
    }
    return arr;
}

console.log(insertionSort([2,6,3,6,3,2,5,3,23,5,6,67,5,3,5,6,432,33,2,33,33,213,424,2,34,4,2,4,2,4,2,432]))
console.log(insertionSort([2,1,9,76,4]))