// arrow function

// comporable to
// function sayHi() {
//    console.log("hello");
//}

// here because function is one line return statement and curly braces are not needed
const sayHi = () => console.log("hello");
sayHi();

// here we are passing in one parameter so we can just have the param name witout ()
const double = value => value * 2;

const num = double(4);
console.log(num);

// arrow function with >1 param
const multiply = (num1,num2) => {
    const result = num1 * num2;
    // further code

    //if more than one line of code a return statement is needed.
    return result
}

const sum = multiply(4, 6);
console.log(sum);


// in order to avoid confusing compiler when returning an object we must first place the object in parenthesis
const object = () => ({ name:'john', age:25})


// callback function
const number = [1,2,3,4,5,6];
const big = number.filter(number => number > 2)
console.log(big);