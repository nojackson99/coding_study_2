// local reviews data
const reviews = [
    {
      id: 1,
      name: "susan smith",
      job: "web developer",
      img:
        "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg",
      text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    },
    {
      id: 2,
      name: "anna johnson",
      job: "web designer",
      img:
        "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg",
      text:
        "Helvetica artisan kinfolk thundercats lumbersexual blue bottle. Disrupt glossier gastropub deep v vice franzen hell of brooklyn twee enamel pin fashion axe.photo booth jean shorts artisan narwhal.",
    },
    {
      id: 3,
      name: "peter jones",
      job: "intern",
      img:
        "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg",
      text:
        "Sriracha literally flexitarian irony, vape marfa unicorn. Glossier tattooed 8-bit, fixie waistcoat offal activated charcoal slow-carb marfa hell of pabst raclette post-ironic jianbing swag.",
    },
    {
      id: 4,
      name: "bill anderson",
      job: "the boss",
      img:
        "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg",
      text:
        "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. ",
    },
  ];

//review page display elements
const personImage = document.getElementById('person-img')
const reviewName = document.getElementById ("author");
const reviewJob = document.getElementById ("job");
const reviewText = document.getElementById("info");
const randomButton = document.querySelector(".random-btn");

//button elements
const prevButton = document.querySelector(".prev-btn");
const nextButton = document.querySelector(".next-btn");

//hold id for updating page display elements
var reviewsID = 0;

//sets new id for setDisplay()
function setNewID(event) {

  //decrements id properly if prev-btn was pushed
  if (event.currentTarget.classList.contains("prev-btn"))
  {
    reviewsID--;
    if (reviewsID == -1) {
      reviewsID = 3;
    }
  //increments id properly if next-btn was pushed
  } else { 
    reviewsID++;
    if (reviewsID == 4) {
      reviewsID = 0;
    }
  }

  //sets display info with updated ID
  setDisplay();
}

//Sets display info from reviews object with ID in reviwsID
function setDisplay() { 
  const item = reviews[reviewsID];

  //sets various elements with info from new ID
  personImage.src = item.img
  reviewName.textContent = item.name;
  reviewJob.textContent = item.job;
  reviewText.textContent = item.text;
}

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

//event listeners for previous and next buttons
prevButton.addEventListener('click',setNewID);
nextButton.addEventListener('click',setNewID);

//sets random id when surprise me is clicked
randomButton.addEventListener('click',function(){
  reviewsID = getRandomNumber(0,4);
  console.log(`random number returned: ${reviewsID}`);
  setDisplay();
})

//calls set diplay once page has loaded
window.addEventListener("DOMContentLoaded", function(){
  setDisplay();
})

