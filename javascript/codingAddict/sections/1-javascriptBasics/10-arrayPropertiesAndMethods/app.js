//Array properties and methods

let names = ['noah', 'alyssa', 'anna', `sophia`, "andrew"]

console.log(names);

//returns length of array
console.log(names.length);

//find element by index
console.log(names[4]);

//always finding last element using array length
console.log(names[names.length - 1]);

//concatenating arrays
const fruits = ['apple', 'cherry', 'banana']

//logic error, this will create a string not an array
let namesAndFruits = `${names} ${fruits}`;
console.log(namesAndFruits)

let namesAndFruits2 = names.concat(fruits);
console.log(namesAndFruits2);

//reverse array
console.log(namesAndFruits2.reverse());

//add to beginning of array
// .shift() will remove first element of array
// .push(<element>) will add to the end of the array
// .pop() will remove form the end of the array
namesAndFruits2.unshift('orange');
console.log(namesAndFruits2);

console.log(`splice shown here`)
//spice - mutates original array
//***AVOID MUTATING ORIGINAL ARRAY IN REACT THIS WILL CAUSE PROBLEMS***
const specificNamesAndFruits = namesAndFruits2.splice(2, 3); //first number is staring index
                                                             //second is desired elements after starting
console.log(namesAndFruits2);
console.log(specificNamesAndFruits);
