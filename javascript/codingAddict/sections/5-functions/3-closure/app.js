// closure

// Basic
/*
function outer() {
    let privateVar = 'secret';
    function inner() {
        console.log(`hello there secret is: ${privateVar}`)
    }
    return inner;
}

outer()();

const value = outer();
console.log(value);
value();
*/

function newAccount(name,initialBalance){
    let balance = initialBalance;
    function showBalance() {
        console.log(`Hey ${name}, your balance is ${balance}`)
    }
    return {showBalance:showBalance};
}

const john = newAccount('john',300)
const bob = newAccount('bob',1000)
const susan = newAccount('susan',500)

john.showBalance();
bob.showBalance();
susan.showBalance();