// Switch statments
// dice values : 1 - 6

//switch statments are a replacement for multiple if statements
//either method works up to coder preference

const dice = 2;

// check if variable dice is equal to case value
switch(dice){
    case 1:
        console.log('you got one')
        break   //tells code to break out of switch statement
    case 2:
        console.log('you got two')
        break
    case 3:
        console.log('you got three')
        break
    case 4:
        console.log('you got four')
        break
    case 5:
        console.log('you got five')
        break
    case 6:
        console.log('you got six')
        break
    //default statment is catch for when all other cases fail
    default:
        console.log('how did you get this number ?!?!')
}


// below is switch statment written with if statements

// if (dice === 1) {
//     console.log('you got one');
// };
// if (dice === 2) {
//     console.log('you got two');
// };
// if (dice < 1 || dice > 6) {
//     console.log('how did you get this number?!?!');
// };