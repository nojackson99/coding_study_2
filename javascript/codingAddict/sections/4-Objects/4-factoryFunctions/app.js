// factory functions

// this function is an object template that will create an object based on values passed in
function createPerson(firstName, lastName) {
    return {
        firstName,
        lastName,
        fullName:function(){
            console.log(
                `My full name is ${this.firstName} ${this.lastName} and I love JS`
            )
        },
    }
}

const noah = createPerson('Noah','Jackson')
noah.fullName()