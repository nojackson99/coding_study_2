// this basics

const john = {
    firstName :'John',
    lastName :'Anderson',
    fullName:function(){
        // this keyword refers to object the keyword is in
        console.log(`My full name is ${this.firstName} ${this.lastName}`)
    }
}

const bob = {
    firstName :'Bob',
    lastName :'Sanders',
    fullName:function(){
        console.log(`My full name is ${this.firstName} ${this.lastName}`)
    }
}

john.fullName();
bob.fullName();