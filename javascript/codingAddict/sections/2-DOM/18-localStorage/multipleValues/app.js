// local storage multiple values
// JSON.stringify(),JSON.parse()

const friends = ['john','peter','bob'];

//json.stringify stores values as array
localStorage.setItem('friends', JSON.stringify(friends));

//values now properly stores friends array
//without JSON functions friends will store as a string in values
//eg. values = 'john,peter,bob'
const values = JSON.parse(localStorage.getItem('friends'));

console.log(values);
console.log(values[0]);

localStorage.setItem('fruits', JSON.stringify(['apple', 'banana', 'haha']))

let fruits;

if (localStorage.getItem('fruits')) {
    fruits = JSON.parse(localStorage.getItem('fruits'));
    console.log(fruits);
}
else {
    fruits = [];
    console.log('nothing in fruits');
}

