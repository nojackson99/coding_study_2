* Objectives
- clicking right button displays next review
- clicking left button displays previous review

* Variables
- picture display element
- name display element
- job display element
- review display element

* Functions
- change object display


PseudoCode

start main {

    //variables
    reviews object array
    store picture display element
    store name display element
    store job display element
    store review display element

    store left chevron button
    store right chevron button

    store id_placeholder = 0

    //functions
    start change review display {

        check current display id

        if left button called this
            decrement id_placeholder
        else
            increment id_placeholder

        if id is 4
            set to 0
        if id is -1
            set to 3

        call set display (pass in id) 

    }

    start set display {

        set name according to id
        set job according to id
        set img according to id
        set text according to id

    }

    left button click event
    right button click event
    call set display (id) 

}