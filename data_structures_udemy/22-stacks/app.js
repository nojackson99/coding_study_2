var stack = []

stack.push("google");
stack.push("instagram");
stack.push("youtube")

// stack implementation with classes/linked lists
class Stack {
    constructor(){
        this.first = null
        this.last = null;
        this.size = 0;
    }
    push(val) {
        let node = new Node(val);

        if (!this.first) {
            this.first = node;
            this.last = node;
        } else {
            let tempNode = this.first;
            this.first = node;
            node.next = tempNode;
        }

        return ++this.size;
    }
    pop() {
        if (!this.first) return null;
        let tempNode = this.first;
        if (this.size === 1) {
            this.first = null;
            this.last = null;
        } else {
            this.first = tempNode.next;
        }
        this.size--;
        return tempNode.val;
    }
}

class Node {
    constructor(val){
        this.value = val;
        this.next = null;
    }
}

let myStack = new Stack();
myStack.push("First In");
myStack.push("Second In");
myStack.push("Last In");
