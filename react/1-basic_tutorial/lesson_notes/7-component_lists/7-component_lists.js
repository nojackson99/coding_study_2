// 7-component_lists

import React from 'react';
import ReactDom from 'react-dom';

// css
import "./index.css";

// variables
// to simplify component rendering we turn book objects into an array
// we use map in the BookList to assist in constructing Book components
const books = [
  {
    img:
      'https://images-na.ssl-images-amazon.com/images/I/811ftjOO5FL._AC_UL302_SR302,200_.jpg',
    title: 'Chicka Chicka Boom Boom',
    author: 'Bill Martin Jr.'
  },
  {
    img:
      'https://images-na.ssl-images-amazon.com/images/I/81EVdWdmOKL._AC_UL604_SR604,400_.jpg',
    title: 'Brown Bear, Brown Bear, What Do You See?',
    author: 'Bill Martin Jr / Eric Carle'
  },
  {
    img:
      'https://images-na.ssl-images-amazon.com/images/I/91cqEsyjd-L._AC_UL604_SR604,400_.jpg',
    title: 'The Very Hungry Catepillar',
    author: 'Eric Carle'
  }
]

function BookList() {
  return (
    <section className='booklist'>
      {/* map allows us to pass in each element of books as a book property into our Book JSX element */}
      {books.map((book) => {
        return <Book book={book}></Book>
      })}
    </section>
  );
}

const Book = (props) => {
  console.log(props);
  //here we are dereferencing the book object property into img, title, and author
  const { img, title, author } = props.book;
  return (
    <article className='book'>
      <img src={img} alt="" />
      <h2>{title}</h2>
      <h4>{author}</h4>
    </article>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));