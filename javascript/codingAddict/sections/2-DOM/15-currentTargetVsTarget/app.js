// current target vs target

const btns = document.querySelectorAll('.btn');


btns.forEach(function(btn){
    btn.addEventListener('click',function(event){
        // .currentTarget with select element that is tied to event listener (here is it btn)
        // console.log(event.currentTarget);
        // event.currentTarget.style.color = 'green';
        console.log('target', event.target);

        //this will only change the element selected by mouse
        event.target.style.color = 'green'
    })
})

