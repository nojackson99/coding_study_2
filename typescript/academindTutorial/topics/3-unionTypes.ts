// union allows for a variable to be given multiple possible types can be more than 2
// syntax as shown: var <varName>: <type1> | <type2>;
function combine(input1: number | string, input2: number | string) {
    
    /*
    this code results in error because typescript is concerned that type of input1 or input2
    may not be addable (ex: if type is a boolean)
    const result = input1 + input2;
    return result;
    */
    
    // error is fixed by confirming that types of input1 and input2 can be added together
    // (ex: number or string)
    let result;
    if (typeof input1 === 'number' && typeof input2 === 'number') {         //check if input 1 and input 2 are numbers
        result = input1 + input2;                                           //then adds combines the variables
    } else {                                                                //if not ensures variables are converted to strings
        result = input1.toString() + input2.toString();                     //and concatenates
    }
    return result;
};

const combinedAges = combine(30, 26);
console.log(combinedAges);

const combinedNames = combine('Max', 'Anna');
console.log(combinedNames);