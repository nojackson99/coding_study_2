//Implicit Type Conversion

const firstName = 'Noah';
const lastName = 'Jackson';
let fullName = firstName + ' ' + lastName;
console.log(fullName);

const number = 4;
const number2 = 10;
const result = number + number2;
console.log(result);

//results in NaN because you can't subtract strings
const value = firstName - lastName;
console.log(value);

//even though number 3 and number 4 are strings subtracting still works
//javascript attempts to convert strings to number to do math
let number3 = '10';
let number4 = '18';
const result2 = number3 - number4; //type conversion here is successful
console.log(result2);              //outputs correct value of -8 to console

//attempting to add these variables will result in '1018' due to string concatenation rules
//even if one variable is string and other is number the result will be a concatenated string
const result3 = number3 + number4;
console.log(result3);