// getAndSetAttribute();

//selecting specific attributes of selected element
const first = document.querySelector('.first');
const idValue = first.getAttribute('id');
// console.log(idValue);

const link = document.getElementById('link');
const showLink = link.getAttribute('href');
// console.log(showLink);

const last = link.nextElementSibling;
console.log(last);

//first parameter is attribute to target and second is the value we want to set
last.setAttribute('class','first')
console.log(last.getAttribute('class'))

//change html element content with js
last.textContent = 'i also have a class fo first';

const links = document.querySelectorAll('.first')
// console.log(links)



