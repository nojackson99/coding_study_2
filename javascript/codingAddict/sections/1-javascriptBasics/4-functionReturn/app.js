//Function returns


//inch to cm translation
//1 inch equals 2.54cm

//variable declarations
const objHeight = 80;
const cmMultiplier = 2.54;

//calculates value in cm and prints to console
function calculate(value){
    const valueInCm = value * cmMultiplier;
    console.log('The value in cm is: ' + valueInCm + ' cm');
    return valueInCm;
};

const width = calculate(100);
const height = calculate(objHeight);
const dimensions = [width, height];
console.log(dimensions);