// 9-import_and_export

import React from 'react';
import ReactDom from 'react-dom';

// css
import "./index.css";

// javascript imports
import {books} from './books'
import Book from './Book'

// JSX function
function BookList() {
  return (
    <section className='booklist'>
      {books.map((book) => {
        return <Book key={book.id} {...book}></Book>
      })}
    </section>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));