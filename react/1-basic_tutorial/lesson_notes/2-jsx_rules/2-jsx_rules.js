/*
    JSX Rules
    return single element
    div / section / article or Fragment
    use camelCase for html attribute
    className instead of class
    close every element
    formatting
*/

function Greeting() {
    return (
      <div>
        <h3>hello people</h3>
        <ul>
          <li>
            <a href='#'></a>
          </li>
        </ul>
      </div>
      // each jsx element must have a single parent div
      // the code below would cause an error
      // <
    );
  }