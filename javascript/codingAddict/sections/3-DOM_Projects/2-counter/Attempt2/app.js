//store button elements in array
//changing to query selector all
// const buttonsArray = [
//     document.querySelector(".reset"),
//     document.querySelector(".increase"),
//     document.querySelector(".decrease")
// ];

buttonsArray = document.querySelectorAll(".btn");
const counterDisplay = document.getElementById("value");

function checkCounter(event){
    //set counter display value to tep
    let temp = counterDisplay.textContent;

    //modify temp depending on button clicked
    if (event.target.textContent == 'increase') {
        temp++;
    } else if (event.target.textContent == 'decrease') {
        temp--;
    } else {
        temp = 0;
    }

    //update counter display color depnding on temp value
    if (temp > 0) {
        counterDisplay.style.color = 'green';
    } else if (temp < 0) {
        counterDisplay.style.color = 'red';
    } else {
        counterDisplay.style.color = 'black';
    }

    //update counter display value
    counterDisplay.textContent = temp; 
}

//set click event listener to each button
buttonsArray.forEach(function(button){
    button.addEventListener('click',checkCounter);
});



