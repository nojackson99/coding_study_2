// constructor functions

// this function is an object template that will create an object based on values passed in
function Person(firstName, lastName){
    this.firstName = firstName;
    this.lastName = lastName;
    this.fullName = function() {
        console.log(
            `My full name is ${this.firstName} ${this.lastName} and I love React`
        )
    }
}

// new keyword creates new object, points to it, and allows for omitting return
const john = new Person('john', 'anderson');

// .constructor points to constructor function of object
console.log(john.constructor)

// objects and arrays also have built in constructor functions
const bob = {};
console.log(bob.constructor);
const list = [];
console.log(list.constructor);