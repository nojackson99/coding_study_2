// traversing the dom
/*
const buttons = document.querySelectorAll(".question-btn")

buttons.forEach(function(btn){
    btn.addEventListener('click',function(){

        //traverse the dom to the question element
        const question = btn.parentElement.parentElement;
        question.classList.toggle("show-text");
    })
})
*/

//using selectors inside the element
const questions = document.querySelectorAll(".question");

questions.forEach(function (question) {
    const btn = question.querySelector('.question-btn');
    btn.addEventListener('click', function(){
        question.classList.toggle("show-text");
    })
})

