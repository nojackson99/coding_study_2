const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
var hexcode = '#';
const myBtn = document.getElementById('btn');
const colorTextDisplay = document.querySelector('.color');

// TODO: move this into a master function file
// generate random number between 0 and colors array length
function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function hexcodeGenerator() {
    hexcode = '#';
    let hexElement = null;

    for (let i = 0; i < 6; i++) {
        hexElement = hex[getRandomNumber(0, hex.length)]
        hexcode = hexcode + hexElement;
    }
}

function buttonCallback() { 
    hexcodeGenerator();
    colorTextDisplay.textContent = hexcode;    //switch display to show what color is in background
    document.body.style.backgroundColor = hexcode;
}

// on click of myBtn buttonCallback function will be executed
myBtn.addEventListener('click',buttonCallback);



