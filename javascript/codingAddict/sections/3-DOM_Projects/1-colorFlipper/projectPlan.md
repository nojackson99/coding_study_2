app.js plan

* Objectives
    - clicking button changes background color
        - color changes to random element in colors array
    - color of background is displayed

* Variables
    - click me button
    - webpage background
    - element where color will be displayed

* Functions
    - random number generator
    - callback for button click event listener

* Pseudocode
main start {

    // variables
    colors array
    set button element to variable
    set background element to variable
    set background color to variable

    button click event function start {

        generate random number
        lookup color element from rand number
        sets background to color element
        display background color element
    }

    create button click event()

}


hex.js plan

* Objectives
    - clicking button changes background color
        - color changes to random hexcode color
    - color of background is displayed

* Variables
    - click me button
    - webpage background
    - element where color will be displayed
    - current hexcode color

* Functions
    - random hexcode generator
    - callback for button click event listener

* Pseudocode
main start {

    // variables
    hexcode storage
    set button element to variable
    set background element to variable
    set background color to variable

    funtion random number generator start {
        generate random number
        return random number
    }

    function hexcode generator start {
        set hexcode storage to null

        for loop start {
            call random number generator
            lookup hex element using rand num return
            if !(hexcode storage) start {
                set hexcode storage equal to hex element
            } else {
                concatenate hexcode storage with hex element
            }
        }

        return hexcode variable
    }

    button click event function start {

        call hexcode generator
        set background to generated hexcode
        display background color element
    }

    create button click event()

}