// 9-import_and_export
import React from 'react'

// JSX component
const Book = ({ img, title, author }) => {
    const clickHandler = () => {
    alert('hello world');
    }
    const complexExample = () => {
      console.log(author);
    }
    return (
      <article className='book'>
        <img src={img} alt="" />
        <h2 onClick={() => alert(title)}>{title}</h2>
        <h4>{author}</h4>
        <button type="button" onClick={clickHandler}>
          reference example
        </button>
        <button type="button" onClick={() => complexExample(author)}>
          more complex example
        </button>
      </article>
    );
  };

export default Book