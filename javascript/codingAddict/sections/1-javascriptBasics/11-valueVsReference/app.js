//Reference vs Value

const number = 1;
let number2 = number;
number2 = 7;

console.log(`the first value is ${number}`)
console.log(`the first value is ${number2}`)

//here we can see assigning a new name to person2.name changes what is stored in person1.name
//this because change is made by reference so changes to .name will change all places where this is stored
//this does not happen with primative data types as shown above
let person = { name: 'bob'};
let person2 = person;
person2.name = `noah`

console.log(`the name of the first person is ${person.name}`);
console.log(`the name of the second peron is ${person2.name}`);

//one way to fix is using ...{var} to copy by value instead of reference
let person3 = { name: 'bob'};
let person4 = { ...person };
person4.name = `noah`

console.log(`the name of the first person is ${person3.name}`);
console.log(`the name of the second peron is ${person4.name}`);