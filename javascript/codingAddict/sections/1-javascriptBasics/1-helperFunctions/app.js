//helper functions

//writes string directly on page
document.write('I am written with document.write')

//shows string in popup box
alert('I am written with alert')

//shows string in console
//this is best for dubugging much more powerful than alert or document.write
console.log('I am written with console.log')