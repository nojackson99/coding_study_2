const names = require('./names');
const sayHi = require('./utils');
const data = require('../alternative_syntax');
console.log(data);

sayHi('susan');
sayHi(names.john);
sayHi(names.peter);
