//can store any data of any type in this variable
let userInput: unknown;
let userName: string;

userInput = 5;
userInput = 'Max';

//userName = userInput; ***ERROR*** since userInput is tyupe unknown you can't store a string variabale
//This shows the difference between unknown and any. If type of userInput was any then userName = userInput would be valid code3

//typescript sees this if check ensuring that userInput will be string and allows value of userInput to be assigned to userName
if (typeof userInput === 'string') {
    userName = userInput;
}

function generateError(message: string, code: number): never { //used to make clear that function should not return anything
    throw { message: message, errorCode: code };               //in this instance this function will throw error and stop execution of code
}

const result = generateError('An error occurred!', 500);
console.log(result);