function binarySearch(arr,val) {
    let start = 0;
    let end = arr.length - 1;
    let mid = null

    while (start < end) {
        mid = Math.floor((start + end) / 2)
        if (arr[mid] === val) {
            return mid;
        } else if (arr[mid] < val){
            start = mid + 1;
        } else {
            end = mid -1;
        }
    }
    return -1
}

console.log(binarySearch([1,2,3,4,5],2));